import React from "react"
import { CommonLayout as Layout } from "./layout"
import "./index.css"

const RootComponent = () => (
    <Layout />
)

export {
    RootComponent
}